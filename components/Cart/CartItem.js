import React from "react";
import {
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  Pressable,
} from "react-native";
import * as Animatable from "react-native-animatable";
import { useNavigation } from "@react-navigation/native";

import { GlobalStyles } from "../../constants/styles";
import { getFormattedDate } from "../../util/date";
import { truncate } from "../../util/truncate";

const CartItem = ({ data, handleQuantityItem, removeItem }) => {
  return (
    <Animatable.View animation="slideInRight" duration={1000}>
      <TouchableHighlight onPress={removeItem} underlayColor={"#ccc"}>
        <View style={styles.orchidItem}>
          <View style={[styles.imageContainer]}>
            <Image
              source={{ uri: data?.image }} // Thay URL bằng đường dẫn hình ảnh thực tế của bạn
              style={styles.image} // Đặt kích thước cho hình ảnh
            />
          </View>
          <View style={styles.textContainer}>
            <View>
              <Text style={[styles.textBase, styles.title]}>
                {truncate(data?.title, 20)}
              </Text>
            </View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 10,
                }}
              >
                <Pressable
                  style={styles.button}
                  onPress={() => {
                    handleQuantityItem(data?.id);
                  }}
                >
                  <Text style={{ ...styles.text }}>-</Text>
                </Pressable>
                <Text style={{ ...styles.text, marginLeft: 10 }}>
                  {data?.quantity}
                </Text>
                <Pressable
                  style={styles.button}
                  onPress={() => {
                    handleQuantityItem(data?.id, true);
                  }}
                >
                  <Text style={styles.text}>+</Text>
                </Pressable>
              </View>
              <Pressable
                style={styles.button}
                onPress={() => {
                  removeItem(data?.id);
                }}
              >
                <Text style={styles.text}>Remove</Text>
              </Pressable>
            </View>

            <View
              style={{
                justifyContent: "flex-end",
                flexDirection: "row",
                flex: 1,
              }}
            >
              <Text style={styles.textBase}>Price: {data?.price}$s</Text>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    </Animatable.View>
  );
};

export default CartItem;
const styles = StyleSheet.create({
  touchContainer: {
    borderRadius: 8,
    marginVertical: 8,
    marginHorizontal: 4,
  },
  orchidItem: {
    padding: 14,
    backgroundColor: GlobalStyles.colors.primary800,
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 8,
    elevation: 3,
    shadowColor: GlobalStyles.colors.gray500,
    shadowRadius: 4,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
  },
  textBase: {
    color: GlobalStyles.colors.primary50,
  },
  textContainer: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "column",
  },
  title: {
    fontSize: 18,
    marginBottom: 4,
    fontWeight: "bold",
    color: GlobalStyles.colors.primary200,
  },
  description: {
    fontSize: 16,
    marginBottom: 4,
  },
  imageContainer: {
    marginRight: 20,
    borderRadius: 4,
    overflow: "hidden",
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 4,
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    elevation: 3,
    height: 30,
    width: 35,
    backgroundColor: "white",
    paddingLefts: 4,
    marginLeft: 10,
  },
  text: {
    fontSize: 10,
    lineHeight: 10,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "black",
  },
});
