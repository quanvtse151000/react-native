import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
  Pressable,
  Alert,
} from "react-native";
import { useIsFocused, useNavigation } from "@react-navigation/native";

import { GlobalStyles } from "../constants/styles";
import { DUMMY_ORCHID } from "../data/dummy-data";
import { useEffect, useMemo, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CartItem from "../components/Cart/CartItem";

function TodooScreen() {
  const [cartData, setCartData] = useState([]);
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const getItems = async () => {
    try {
      const res = await AsyncStorage.getItem("addToCart");
      const data = JSON.parse(res) ?? [];
      const nextData = DUMMY_ORCHID.reduce((prev, curr) => {
        const item = data?.find(({ id }) => curr?.id === id);
        return item ? [...prev, { ...curr, ...item }] : prev;
      }, []);
      setCartData(nextData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    if (isFocused) {
      getItems();
    }
  }, [isFocused]);
  const totalValue = useMemo(
    () =>
      cartData?.reduce((prev, curr) => prev + curr?.quantity * curr?.price, 0),
    [cartData]
  );
  const handleQuantityItem = (id, isAdd = false) => {
    const newCart = cartData?.reduce((prev, curr) => {
      if (curr?.id === id) {
        if (isAdd) {
          return [...prev, { ...curr, quantity: curr?.quantity + 1 }];
        }
        return curr?.quantity === 1
          ? prev
          : [...prev, { ...curr, quantity: curr?.quantity - 1 }];
      }
      return [...prev, curr];
    }, []);
    setCartData(newCart);
  };
  const handleSave = async () => {
    try {
      await AsyncStorage.setItem("addToCart", JSON.stringify(cartData));
      Alert.alert("Save to Cart Success", "", [
        {
          text: "Okay",
        },
      ]);
    } catch (error) {
      console.log(error);
    }
  };
  const removeItem = (id) => {
    setCartData(cartData?.filter((ite) => !(ite?.id === id)));
  };

  return (
    <View style={styles.container}>
      <View>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={navigation.openDrawer}>
            <Image
              source={require("../assets/1.png")}
              style={styles.imageDrawer}
            />
          </TouchableOpacity>

          <View style={styles.headerText}>
            <View style={{ width: "70%" }}>
              <Text style={styles.textWelcome}>Welcome to</Text>
              <Text style={styles.textShop}>Cart</Text>
            </View>
            <View style={{ width: "30%", alignItems: "flex-end" }}>
              <Image
                source={require("../assets/user.jpg")}
                style={styles.imageUser}
              />
            </View>
          </View>
        </View>
        <View>
          {cartData?.map((item) => (
            <CartItem
              key={item?.id}
              data={item}
              handleQuantityItem={handleQuantityItem}
              removeItem={removeItem}
            />
          ))}
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Pressable style={styles.button} onPress={handleSave}>
            <Text style={{ ...styles.text }}>Save</Text>
          </Pressable>
          <Text style={{ fontSize: 24, marginTop: 10, fontWeight: "bold" }}>
            Total: {totalValue}
          </Text>
        </View>
      </View>
    </View>
  );
}

export default TodooScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  headerContainer: {
    backgroundColor: GlobalStyles.colors.primary700,
    height: Dimensions.get("window").height / 4,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingHorizontal: 20,
  },
  imageDrawer: {
    height: 10,
    width: 20,
    marginTop: 60,
  },
  headerText: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 25,
    width: "100%",
  },
  textWelcome: { fontSize: 24, color: "#FFF", fontWeight: "bold" },
  textShop: {
    fontSize: 28,
    color: GlobalStyles.colors.primary200,
    fontWeight: "bold",
  },
  imageUser: {
    height: 60,
    width: 60,
    borderRadius: 99,
  },
  linearContainer: {
    left: 0,
    right: 0,
    height: 90,
    marginTop: -45,
  },
  searchContainer: {
    backgroundColor: "#FFF",
    paddingVertical: 14,
    paddingHorizontal: 20,
    marginHorizontal: 20,
    borderRadius: 15,
    marginTop: 25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    elevation: 3,
    height: 40,
    width: 50,
    backgroundColor: "white",
    paddingLefts: 4,
    marginLeft: 10,
  },
});
